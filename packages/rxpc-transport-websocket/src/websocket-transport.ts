import {LastDisconnect, RpcTransport, RpcTransportDisconnectReason, RpcTransportStatus} from '@epicentric/rxpc';
import {BehaviorSubject, EMPTY, fromEvent, Observable, race} from 'rxjs';
import {outerDependentSwitchMap} from '@epicentric/utilbelt';
import {distinctUntilChanged, filter, map, mapTo, share, take, timeoutWith} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';

interface WebsocketTransportOptions
{
    createConnection?: () => WebSocket;
    // TODO: create an interface that accepts both a browser websocket and a node websocket
    socket?: any;
}

// Here for cross-platform compatibility
const enum WebsocketState
{
    Connecting  = 0,
    Open        = 1,
    Closing     = 2,
    Closed      = 3
}

export class WebsocketTransport implements RpcTransport
{
    public readonly message: Observable<unknown>;
    public readonly error: Observable<any>;
    public readonly statusChange: BehaviorSubject<RpcTransportStatus>;
    public readonly connectedChange: Observable<boolean>;
    public readonly closed: Observable<void>;
    public readonly disconnected: Observable<{ code: number, reason: string }>;
    
    public lastDisconnect: LastDisconnect = {
        reason: RpcTransportDisconnectReason.None,
        details: {}
    };
    
    public get reconnectionSupported(): boolean
    {
        return !!this.createConnection && this.status !== RpcTransportStatus.Closed;
    }

    public get status(): RpcTransportStatus
    {
        return this.statusChange.getValue();
    }
    
    private createConnection?: () => WebSocket;
    private socket?: WebSocket|null = null;
    
    public constructor(options: WebsocketTransportOptions)
    {
        if (!options.createConnection && !options.socket)
            throw new Error(`Must specify either a socket or a createConnection callback`);
        
        ({
            socket: this.socket,
            createConnection: this.createConnection
        } = options);

        this.statusChange = new BehaviorSubject(this.socket ? RpcTransportStatus.Connected : RpcTransportStatus.New);

        // Provide a simplified observable for the following chains
        this.connectedChange = this.statusChange.pipe(
            map(status => status === RpcTransportStatus.Connected),
            distinctUntilChanged()
        );
        
        this.message = this.connectedChange.pipe(
            // This will complete on closed status, because the outer observable will complete and the inner observable
            // will switch to EMPTY which will complete instantly as well
            outerDependentSwitchMap(connected => connected ? fromEvent(this.socket!, 'message') : EMPTY),
            map((event: any) => event.data || event),
            share()
        );

        this.error = this.connectedChange.pipe(
            // This will complete on closed status, because the outer observable will complete and the inner observable
            // will switch to EMPTY which will complete instantly as well
            outerDependentSwitchMap(connected => connected ? fromEvent(this.socket!, 'error') : EMPTY),
            share()
        );

        this.disconnected = this.connectedChange.pipe(
            outerDependentSwitchMap(connected => connected ? fromEvent<[number, string]|{ code: number, reason: string }>(this.socket!, 'close') : EMPTY),
            map(data => Array.isArray(data)
                ? { code: data[0], reason: data[1] } // Node
                : { code: data.code, reason: data.reason } // Browser
            ),
            share()
        );

        this.disconnected.subscribe(details => {
            this.lastDisconnect = {
                reason: 4401 === details.code || 4403 === details.code
                    ? RpcTransportDisconnectReason.ConnectionDenied
                    : RpcTransportDisconnectReason.Unknown,
                details
            };
            
            this.statusChange.next(RpcTransportStatus.Disconnected);
        });

        this.closed = this.statusChange.pipe(
            filter(status => status === RpcTransportStatus.Closed),
            mapTo(undefined)
        );
    }
    
    public async connect(): Promise<void>
    {
        if (this.status === RpcTransportStatus.Connected)
            return;

        this.lastDisconnect = { reason: RpcTransportDisconnectReason.None, details: {} };

        try
        {
            await this.doConnect();
        }
        catch (err)
        {
            if (this.socket)
                this.socket.close();

            this.socket = null;
            this.statusChange.next(RpcTransportStatus.Disconnected);

            throw err;
        }
    }
    
    public disconnect(code?: number, reason?: string): void
    {
        if (this.socket)
            this.socket.close(code, reason);
    }

    public close(code?: number, reason?: string): void
    {
        this.disconnect(code, reason);
        
        this.statusChange.next(RpcTransportStatus.Closed);
        this.statusChange.complete();
    }

    public send(data: unknown): boolean
    {
        if (this.status !== RpcTransportStatus.Connected || !this.socket)
            return false;
        
        this.socket.send(data as any);
        return true;
    }
    
    private async doConnect(): Promise<void>
    {
        const ws = this.createConnection ? this.createConnection() : this.socket!;

        if (ws.readyState === WebsocketState.Closed || ws.readyState === WebsocketState.Closing)
            throw new Error(`Got an already closing socket`);

        if (ws.readyState === WebsocketState.Connecting)
        {
            const connectionResult = await race(
                fromEvent(ws, 'open'),
                fromEvent(ws, 'error'),
                fromEvent(ws, 'close')
            ).pipe(
                take(1),
                timeoutWith(10000, of({ type: 'timeout' }))
            ).toPromise();

            switch (connectionResult.type)
            {
                case 'error':
                    throw new Error(`Error while connecting to peer`);
                case 'close':
                    throw new Error(`Connection reset while connecting to peer`);
                case 'timeout':
                    throw new Error(`Timeout while connecting to peer`);
            }
        }

        // If we're here, the readyState is CONNECTED and we're good to proceed
        this.socket = ws;
        this.statusChange.next(RpcTransportStatus.Connected);
    }
}