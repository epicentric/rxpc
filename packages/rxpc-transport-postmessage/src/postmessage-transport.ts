import {LastDisconnect, RpcTransport, RpcTransportDisconnectReason, RpcTransportStatus} from '@epicentric/rxpc';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {filter, map, takeUntil} from 'rxjs/operators';

export interface PostmessageTransportOptions
{
    currentWindow: Window;
    targetWindow: Window;
}

export enum PostmessageType
{
    Message       = 'rxpcMessage',
    Disconnect    = 'rxpcDisconnect',
    Connect       = 'rxpcConnect'
}

export class PostmessageTransport implements RpcTransport
{
    public readonly closed: Subject<void> = new Subject;
    public readonly connectedChange: BehaviorSubject<boolean> = new BehaviorSubject(true);
    public readonly disconnected: Subject<any> = new Subject;
    public readonly error: Subject<any> = new Subject;
    public readonly reconnectionSupported = true;
    public readonly statusChange: BehaviorSubject<RpcTransportStatus> = new BehaviorSubject(RpcTransportStatus.Connected);
    public message: Observable<unknown>;
    public lastDisconnect: LastDisconnect = { reason: RpcTransportDisconnectReason.None, details: {} };
    public currentWindow: Window;
    public targetWindow: Window;
    public sendingEnabled: boolean = true;
    
    public get status(): RpcTransportStatus
    {
        return this.statusChange.getValue();
    }
    
    private isClosed: boolean = false;

    public constructor(options: PostmessageTransportOptions)
    {
        this.currentWindow = options.currentWindow;
        this.targetWindow = options.targetWindow;
        
        this.message = fromEvent<MessageEvent>(this.currentWindow, 'message').pipe(
            filter(event => event.data && event.data.data && event.data.type === PostmessageType.Message),
            map(event => event.data.data),
            takeUntil(this.closed)
        );

        fromEvent<MessageEvent>(this.currentWindow, 'message').pipe(
            filter(event => event.data && event.data.data && event.data.type === PostmessageType.Disconnect),
            takeUntil(this.closed)
        ).subscribe(() => this.statusChange.next(RpcTransportStatus.Disconnected));
    }
    
    public close(): void
    {
        if (this.isClosed)
            return;

        this.disconnect();
        
        this.isClosed = true;
        this.closed.next();
        this.closed.complete();
        this.connectedChange.complete();
        this.disconnected.complete();
        this.error.complete();
        this.statusChange.complete();
    }

    public async connect(): Promise<void>
    {
        if (this.isClosed)
            throw new Error(`Can't connect with a closed transport`);
        
        this.sendMessage(PostmessageType.Connect);
    }
    
    public disconnect(): void
    {
        if (this.status !== RpcTransportStatus.Connected)
            return;

        this.sendMessage(PostmessageType.Disconnect);
        this.statusChange.next(RpcTransportStatus.Disconnected);
    }

    public send(data: unknown): boolean
    {
        if (this.isClosed)
            return false;
        
        this.sendMessage(PostmessageType.Message, data);
        return true;
    }
    
    private sendMessage(type: PostmessageType, data?: unknown)
    {
        if (!this.sendingEnabled)
            return;

        this.targetWindow.postMessage({ type, data }, '*');
    }
}