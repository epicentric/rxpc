export interface BackoffFunction<TArgs extends any[] = any[], TResult = any>
{
    (tryFunc: () => Promise<TResult>): Promise<TResult> & { stop(): void };
}