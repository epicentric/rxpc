import {delay, Deferred} from '@epicentric/utilbelt';

export interface ExponentialBackoffOptions<T>
{
    tryFunction: () => Promise<T>;
    minSeconds?: number;
    maxSeconds?: number;
    maxTries?: number;
}

export interface Backoff<T>
{
    isActive: boolean;
    start(): Promise<T>;
    stopWithResolve(value?: T): void;
    stopWithReject(err?: any): void;
}

export class ExponentialBackoff<T> implements Backoff<T>
{
    public get isActive(): boolean
    {
        return !!this.currentDeferred;
    }
    
    private currentDeferred: Deferred<T>|null = null;
    private shortCircuited: boolean = false;
    
    private readonly tryFunction: () => Promise<T>;
    private readonly minSeconds: number;
    private readonly maxSeconds: number;
    private readonly maxTries: number;
    
    public constructor(options: ExponentialBackoffOptions<T>)
    {
        this.tryFunction = options.tryFunction;
        this.minSeconds = undefined !== options.minSeconds ? options.minSeconds : 1;
        this.maxSeconds = undefined !== options.maxSeconds ? options.maxSeconds : Number.POSITIVE_INFINITY;
        this.maxTries = undefined !== options.maxTries ? options.maxTries : Number.POSITIVE_INFINITY;
    }
    
    public async start(): Promise<T>
    {
        if (this.isActive)
            throw new Error(`Backoff is already active`);
        
        this.shortCircuited = false;
        this.currentDeferred = new Deferred();
        try
        {
            let currentWait = this.minSeconds * 1000;

            for (let tryCount = 0; tryCount < this.maxTries; tryCount++)
            {
                try
                {
                    return await Promise.race([
                        this.tryFunction(),
                        this.currentDeferred!.promise as any as Promise<T>
                    ]);
                }
                catch (err)
                {
                    if (this.shortCircuited)
                        throw err;

                    currentWait = Math.min(currentWait + (1 + Math.random()) * currentWait, this.maxSeconds * 1000);
                    console.log(`Starting wait of ${(currentWait / 1000).toFixed(2)}s`);
                    await delay(currentWait);
                    console.log('Finished waiting');
                }
            }
            
            throw new Error(`Max try count of ${this.maxTries} reached`);
        }
        finally
        {
            this.currentDeferred = null;
            this.shortCircuited = false;
        }
    }
    
    public stopWithResolve(value?: T): void
    {
        this.shortCircuited = true;
        this.currentDeferred!.resolve(value);
    }
    
    public stopWithReject(err?: any): void
    {
        this.shortCircuited = true;
        this.currentDeferred!.reject(err);
    }
}