import 'reflect-metadata';
import {NoEarlyExitProps, ValidationFunction} from '@epicentric/validational';
import {RpcRequest} from './rpc-handler';

const RPC_CALLABLE = Symbol('RPC_CALLABLE');

export interface RequestValidationProps<TExtras> extends NoEarlyExitProps
{
    request: RpcRequest<TExtras>;
}

export interface RpcCallableOptions<TExtras>
{
    meta?: { [key: string]: any; };
    validateParams?: ValidationFunction<RequestValidationProps<TExtras>>;
}

export function getRpcCallables<TExtras>(target: any): { name: string, options: RpcCallableOptions<TExtras> }[]
{
    return Reflect.getMetadata(RPC_CALLABLE, target) || [];
}

export function rpcCallable<TExtras = {}>(options: RpcCallableOptions<TExtras> = {})
{
    return function(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
        const current = getRpcCallables<TExtras>(target);
        current.push({ name: propertyKey, options });
        Reflect.defineMetadata(RPC_CALLABLE, current, target);
    };
}