import {BinaryId} from '@epicentric/binary-id';

export enum RpcMessageType
{
    Ping        = 'ping',
    Receipt     = 'receipt',
    Call        = 'call',
    Next        = 'next',
    Complete    = 'complete',
    Error       = 'error',
    Unsubscribe = 'unsubscribe'
}

export type RpcMessageId = BinaryId;

export interface RpcMessageMetadata
{
    [key: string]: any;
}

export interface RpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType;
    originalId?: RpcMessageId;
    metadata?: RpcMessageMetadata;
}

export interface PingRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Ping;
    metadata?: RpcMessageMetadata;
}

export interface ReceiptRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Receipt;
    originalId: RpcMessageId;
    metadata?: RpcMessageMetadata;
}

export interface CallRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Call;
    name: string;
    args: unknown[];
    metadata?: RpcMessageMetadata;
}

export interface NextRpcMessage<TValue = any>
{
    id: RpcMessageId;
    type: RpcMessageType.Next;
    originalId: RpcMessageId;
    value: TValue;
    metadata?: RpcMessageMetadata;
}

export interface CompleteRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Complete;
    originalId: RpcMessageId;
    metadata?: RpcMessageMetadata;
}

export interface ErrorRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Error;
    originalId?: RpcMessageId;
    error: Error;
    metadata?: RpcMessageMetadata;
}

export interface UnsubscribeRpcMessage
{
    id: RpcMessageId;
    type: RpcMessageType.Unsubscribe;
    originalId: RpcMessageId;
    metadata?: RpcMessageMetadata;
}