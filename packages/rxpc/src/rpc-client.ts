import {RpcEncoder} from './rpc-encoder';
import {ErrorRpcMessage, RpcMessage, RpcMessageType} from './rpc-message';
import {BehaviorSubject, EMPTY, interval, Observable, Subject, throwError} from 'rxjs';
import {filter, map, mapTo, share, take, timeoutWith} from 'rxjs/operators';
import {ExponentialBackoff} from './backoff/exponential-backoff';
import {delay, outerDependentSwitchMap} from '@epicentric/utilbelt';
import {RpcTransport, RpcTransportStatus} from './rpc-transport';
import {BinaryIdGenerator, equalsBinaryId} from '@epicentric/binary-id';
import {Overwrite} from 'utility-types';

export interface RpcClientOptions
{
    encoder: RpcEncoder;
    idGenerator: BinaryIdGenerator;
    reconnectionEnabled?: boolean;
    transport: RpcTransport;
    pingEnabled?: boolean;
}

export enum RpcClientStatus
{
    None,
    Connecting,
    Ready,
    Closed
}

export enum DebugEventType
{
    IncomingMessage,
    OutgoingMessage,
    StartConnecting,
    FinishConnecting,
    Error
}

export interface MessageDebugEvent
{
    type: DebugEventType.IncomingMessage | DebugEventType.OutgoingMessage;
    message: RpcMessage;
}

export interface ConnectingDebugEvent
{
    type: DebugEventType.StartConnecting | DebugEventType.FinishConnecting;
}

export interface ErrorDebugEvent
{
    type: DebugEventType.Error;
    error: Error;
}

export type DebugEvent = MessageDebugEvent | ConnectingDebugEvent | ErrorDebugEvent;

export type SendRpcMessage = Overwrite<RpcMessage, { id?: RpcMessage['id'] }>;

// TODO: protect against duplicate messages, eg. with a set of last received messages
export class RpcClient
{
    public readonly debugEvent: Subject<DebugEvent> = new Subject;
    public readonly message: Observable<RpcMessage>;

    public readonly transport: RpcTransport;
    public readonly encoder: RpcEncoder;
    public readonly idGenerator: BinaryIdGenerator;
    
    public readonly status: BehaviorSubject<RpcClientStatus> = new BehaviorSubject(RpcClientStatus.None);
    
    public reconnectionEnabled: boolean;

    private backoff: ExponentialBackoff<void> = new ExponentialBackoff({
        tryFunction: this.tryConnect.bind(this),
        minSeconds: 1,
        maxSeconds: 45
    });

    public constructor(options: RpcClientOptions)
    {
        ({
            encoder: this.encoder,
            idGenerator: this.idGenerator,
            transport: this.transport
        } = options);
        
        this.reconnectionEnabled = undefined === options.reconnectionEnabled || options.reconnectionEnabled;
        
        const pingEnabled = undefined === options.pingEnabled || options.pingEnabled;

        this.message = this.transport.message.pipe(
            map((data: any) => this.encoder.decode(data)),
            share()
        );

        if (pingEnabled)
        {
            // Send pings at regular intervals
            this.transport.connectedChange.pipe(
                outerDependentSwitchMap(connected => connected ? interval(20000) : EMPTY)
            ).subscribe(() => this.sendMessage({ type: RpcMessageType.Ping }));
        }

        this.message.subscribe(message => this.onMessage(message));
        this.transport.disconnected.subscribe(() => this.onDisconnected());
        this.transport.closed.subscribe(() => this.status.next(RpcClientStatus.Closed));
    }

    public connect(): Promise<void>
    {
        if (this.status.getValue() === RpcClientStatus.Closed)
            throw new Error(`Can't connect with a transport in the Closed state`);
        
        if (this.transport.reconnectionSupported && this.reconnectionEnabled)
            return this.backoff.start();
        else
            return this.tryConnect();
    }

    public async send<TMessage extends SendRpcMessage>(message: TMessage): Promise<void>
    {
        switch (this.status.getValue())
        {
            case RpcClientStatus.None:
            case RpcClientStatus.Connecting:
                await this.status.pipe(
                    filter(status => status === RpcClientStatus.Ready),
                    timeoutWith(30000, throwError(new Error(`Timeout while trying to send message`))),
                    take(1)
                ).toPromise();
                break;
            case RpcClientStatus.Closed:
                // If the client is closed, return as there's nothing we can do. Could be changed to an error if needed,
                // for now, this is less intrusive.
                return;
        }
        
        this.sendMessage(message);
        
        const receiptPromise = this.message.pipe(
            filter(incomingMesage => incomingMesage.type === RpcMessageType.Receipt
                && equalsBinaryId(incomingMesage.originalId, message.id)
            ),
            take(1),
            mapTo('receipt') // Erase result
        ).toPromise();
        
        const disconnectPromise = this.transport.disconnected.pipe(
            take(1),
            mapTo('disconnected')
        ).toPromise();
        
        const timeoutPromise = delay(10000, 'timeout');
        
        const result = await Promise.race([
            receiptPromise,
            disconnectPromise,
            timeoutPromise
        ]);
        
        switch (result)
        {
            case 'disconnected':
                throw new Error(`Disconnected while waiting for receipt`);
            case 'timeout':
                throw new Error(`Timeout while waiting for receipt`);
            default:
                break; // Great success!
        }
    }

    private sendMessage(message: SendRpcMessage): boolean
    {
        if (this.status.getValue() !== RpcClientStatus.Ready)
            return false;
        
        if (!message.id)
            message.id = this.idGenerator.create();

        this.debugEvent.next({ type: DebugEventType.OutgoingMessage, message: message as RpcMessage });
        this.transport.send(this.encoder.encode(message as RpcMessage));
        return true;
    }

    private async tryConnect(): Promise<void>
    {
        try
        {
            this.debugEvent.next({ type: DebugEventType.StartConnecting });
    
            if (this.status.getValue() === RpcClientStatus.Ready || this.status.getValue() === RpcClientStatus.Closed)
                return;
            
            this.status.next(RpcClientStatus.Connecting);
            
            await this.transport.connect();

            this.status.next(RpcClientStatus.Ready);
        }
        catch (err)
        {
            let newStatus: RpcClientStatus;
            
            if (this.transport.status === RpcTransportStatus.Closed)
                newStatus = RpcClientStatus.Closed;
            else
                newStatus = RpcClientStatus.Connecting;
            
            this.status.next(newStatus);

            throw err;
        }
        finally
        {
            this.debugEvent.next({ type: DebugEventType.FinishConnecting })
        }
    }
    
    private onMessage(message: RpcMessage): void
    {
        if (message.type !== RpcMessageType.Ping)
            this.debugEvent.next({ type: DebugEventType.IncomingMessage, message });

        if (message.type !== RpcMessageType.Receipt && message.id)
        {
            this.sendMessage({
                type: RpcMessageType.Receipt,
                originalId: message.id
            });
        }

        if (message.type === RpcMessageType.Error)
        {
            this.debugEvent.next({ type: DebugEventType.Error, error: (message as ErrorRpcMessage).error });
        }
    }
    
    private async onDisconnected(): Promise<void>
    {
        try
        {
            // Start reconnect if we can and it's not handled by backoff
            if (this.reconnectionEnabled && this.transport.reconnectionSupported)
            {
                if (!this.backoff.isActive)
                {
                    this.status.next(RpcClientStatus.Connecting);
                    await this.connect();
                }
            }
            else
            {
                this.transport.close();
            }
        }
        catch (err)
        {
            this.debugEvent.next({ type: DebugEventType.Error, error: err });
        }
    }
}