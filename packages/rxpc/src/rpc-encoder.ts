import {RpcMessage} from './rpc-message';

export interface RpcEncoder
{
    encode(message: RpcMessage): any;
    decode(data: any): RpcMessage;
}