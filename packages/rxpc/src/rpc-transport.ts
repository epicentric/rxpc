import {Observable} from 'rxjs';

export enum RpcTransportStatus
{
    New,
    Disconnected,
    Connected,
    Closed
}

export enum RpcTransportDisconnectReason
{
    None,
    ConnectionDenied,
    Unknown,
}

export interface LastDisconnect
{
    reason: RpcTransportDisconnectReason;
    details: { [key: string]: any };
}

export interface RpcTransport
{
    status: RpcTransportStatus;
    message: Observable<unknown>;
    statusChange: Observable<RpcTransportStatus>;
    connectedChange: Observable<boolean>;
    error: Observable<any>;
    closed: Observable<void>;
    disconnected: Observable<any>;
    reconnectionSupported: boolean;
    
    lastDisconnect: LastDisconnect;

    connect(): Promise<void>;
    disconnect(): void;
    close(): void;
    send(data: unknown): boolean;
}