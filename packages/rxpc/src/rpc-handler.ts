import {EMPTY, from, isObservable, Notification, Observable, of, Subject, throwError} from 'rxjs';
import {dematerialize, filter, finalize, map, share, switchAll, take, takeUntil} from 'rxjs/operators';
import {
    RpcMessage,
    CallRpcMessage,
    ErrorRpcMessage,
    NextRpcMessage,
    RpcMessageId,
    RpcMessageType, RpcMessageMetadata
} from './rpc-message';
import {RpcClient, SendRpcMessage} from './rpc-client';
import {getRpcCallables, RpcCallableOptions} from './decorators';
import {HookEmitter} from '@epicentric/hook-emitter';
import {isPromise} from 'rxjs/internal-compatibility';
import {equalsBinaryId} from '@epicentric/binary-id';
import {tap} from 'rxjs/internal/operators/tap';

export type RpcFunctionArgs = unknown[];
export type RpcFunction = (...args: RpcFunctionArgs) => undefined|object|Promise<object>|Observable<object>;
export type RpcReceiverFunction<T, TExtras> = (request: RpcRequest<TExtras>, ...args: RpcFunctionArgs) => Observable<T>|Promise<T>|T|void;

export type ExtrasGetter<TExtras> = (client: RpcClient, name: string, args: unknown[]) => TExtras;

export enum RpcHookType
{
    PreCall,
    PreResolve,
    PostResolve,
    PostCall
}

export interface RpcRequest<TExtras = {}>
{
    parent: RpcRequest<TExtras>|null;
    client: RpcClient|null;
    isLocal: boolean;
    name: string;
    args: RpcFunctionArgs;
    metadata?: RpcMessageMetadata;
    handler: RpcHandler<TExtras>;
    extras: TExtras;
}

export interface HookEventArgs<T, TExtras>
{
    request: RpcRequest<TExtras>;
    callableOptions: RpcCallableOptions<TExtras>|null;
    response: null|Observable<T>;
}

export interface RpcHookOptions
{
    type: RpcHookType;
    filter?: RegExp;
    priority?: number;
}

export class RpcHandler<TExtras = {}>
{
    public readonly hooks: HookEmitter<RpcHookType, HookEventArgs<any, TExtras>, RpcHookOptions> = new HookEmitter(Symbol('RPC_HOOK'));

    private readonly remoteUnsubscribe = new Subject<{ client: RpcClient, id: RpcMessageId }>();
    private readonly incomingMessage = new Subject<{ message: RpcMessage, client: RpcClient }>();
    private readonly functions: Map<string, { function: RpcReceiverFunction<any, TExtras>, options: RpcCallableOptions<TExtras> }> = new Map;

    public async handle(client: RpcClient, message: RpcMessage, extras: TExtras): Promise<void>
    {
        this.incomingMessage.next({ message, client });
        
        switch (message.type)
        {
            case RpcMessageType.Call:
                await this.handleRemoteFunctionCall(client, message as CallRpcMessage, extras);
                break;
            case RpcMessageType.Unsubscribe:
                if (message.originalId)
                    this.handleUnsubscribe(client, message.originalId);
                break;
        }
    }
    
    public register(name: string, handler: RpcReceiverFunction<any, TExtras>, options?: RpcCallableOptions<TExtras>): void
    {
        this.functions.set(name, { function: handler, options: options || {} });
    }
    
    public registerClass(namespace: string, instance: { [key: string]: any }): void
    {        
        const proto = Object.getPrototypeOf(instance);
        const callables = getRpcCallables<TExtras>(proto);

        for (const callable of callables)
        {
            const func = (instance[callable.name] as RpcFunction).bind(instance);
    
            this.register(`${namespace}.${callable.name}`, func, callable.options);
        }
    }
    
    public createProxyClass<T extends object>(client: RpcClient, namespace: string,
                                              extrasGetter: ExtrasGetter<TExtras>, promiseFunctions?: string[]): T
    {
        const promiseFunctionSet = new Set(promiseFunctions);
        
        return new Proxy({} as { [name: string]: Function }, {
            // When somebody requests a property, return a function that will forward the request to the remote side as
            // a function call
            get: (target, property: string) => {
                let cachedMethod = target[property];
                
                if (!cachedMethod)
                {
                    const methodName = property.toString();
                    const remoteName = `${namespace}.${methodName}`;
                    
                    if (promiseFunctionSet.has(methodName))
                    {
                        cachedMethod = (...args: unknown[]) => {
                            const extras = extrasGetter(client, remoteName, args);
                            
                            return this.callRemote(client, remoteName, args, extras).pipe(take(1)).toPromise();
                        };
                    }
                    else
                    {
                        cachedMethod = (...args: unknown[]) => {
                            const extras = extrasGetter(client, remoteName, args);

                            return this.callRemote(client, remoteName, args, extras);
                        };
                    }

                    target[property] = cachedMethod;
                }
                
                return cachedMethod;
            }
        }) as T;
    }
    
    public callRemote<T = any>(client: RpcClient, name: string, args: unknown[], extras: TExtras,
                               metadata?: RpcMessageMetadata): Observable<T>
    {
        return from(this.sendRemoteCall<T>(client, name, args, extras, metadata)).pipe(
            switchAll()
        );
    }
    
    public callLocal<T = any>(name: string, args: RpcFunctionArgs, extras: TExtras, parentRequest?: RpcRequest<TExtras>,
                              metadata?: RpcMessageMetadata): Observable<T>
    {
        const request: RpcRequest<TExtras> = {
            parent: parentRequest || null,
            handler: this,
            client: parentRequest ? parentRequest.client : null,
            name,
            args,
            metadata: metadata || (parentRequest && parentRequest.metadata),
            isLocal: parentRequest ? parentRequest.isLocal : true,
            extras
        };
        
        let obs = from(this.doFunctionCall(request)).pipe(
            switchAll()
        );
        
        if (request.client)
        {
            obs = obs.pipe(takeUntil(request.client.transport.closed));
        }
        
        return obs.pipe();
    }
    
    public callLocalPromise<T = any>(name: string, args: RpcFunctionArgs, extras: TExtras, parentRequest?: RpcRequest<TExtras>): Promise<T>
    {
        const obs = this.callLocal(name, args, extras, parentRequest);
        return obs.pipe(take(1)).toPromise();
    }
    
    private async sendRemoteCall<T>(client: RpcClient, name: string, args: unknown[], extras: TExtras,
                                    metadata?: RpcMessageMetadata): Promise<Observable<T>>
    {
        const hookEventArgs: HookEventArgs<any, TExtras> = {
            request: {
                parent: null,
                isLocal: true,
                args,
                name,
                client,
                handler: this,
                extras,
                metadata
            },
            response: null,
            callableOptions: null
        };

        await this.hooks.emit(RpcHookType.PreCall, hookEventArgs);
        
        if (hookEventArgs.response)
            return hookEventArgs.response;

        const id = await client.idGenerator.create();
        const request = hookEventArgs.request;
        await this.sendMessage(client, {
            id,
            type: RpcMessageType.Call,
            name: request.name,
            args: request.args,
            metadata: request.metadata
        });

        let observableClosed = false;
        return this.incomingMessage.pipe(
            // Only listen for this client's messages with this reply id
            filter(message => message.client === client
                && 'originalId' in message.message
                && equalsBinaryId(message.message.originalId, id)
                && [RpcMessageType.Next, RpcMessageType.Error, RpcMessageType.Complete].includes(message.message.type)
            ),
            map(message => {
                switch (message.message.type)
                {
                    case RpcMessageType.Next:
                        // The any cast fixes a type error in takeUntil down the pipe 
                        return Notification.createNext((message.message as NextRpcMessage).value);
                    case RpcMessageType.Error:
                        observableClosed = true;
                        return Notification.createError((message.message as ErrorRpcMessage).error);
                    case RpcMessageType.Complete:
                        observableClosed = true;
                        return Notification.createComplete();
                    default:
                        throw new Error('Unknown message');
                }
            }),
            dematerialize(),
            // If this observable ends for any reason, send an 'unsubscribe' to the other side
            finalize(() => {
                if (!observableClosed)
                {
                    this.sendMessage(client, {
                        type: RpcMessageType.Unsubscribe,
                        originalId: id
                    });
                    observableClosed = true;
                }
            }),
            takeUntil(client.transport.closed),
            share()
        );
    }
    
    private async doFunctionCall(request: RpcRequest<TExtras>): Promise<Observable<any>>
    {
        try
        {
            const hookEventArgs: HookEventArgs<any, TExtras> = {
                request: request,
                callableOptions: null,
                response: null
            };
            
            await this.hooks.emit(RpcHookType.PreResolve, hookEventArgs);

            if (hookEventArgs.response)
                return hookEventArgs.response;

            const callable = this.functions.get(request.name);
            if (!callable)
                return throwError(new Error(`Unknown function '${request.name}'`));

            hookEventArgs.callableOptions = callable.options;
            await this.hooks.emit(RpcHookType.PostResolve, hookEventArgs);

            if (hookEventArgs.response)
                return hookEventArgs.response;

            const functionResult = callable.function(request, ...request.args);
            
            if (undefined === functionResult)
                hookEventArgs.response = EMPTY;
            else if (isObservable(functionResult))
                hookEventArgs.response = functionResult;
            else if (isPromise(functionResult))
                hookEventArgs.response = from(functionResult);
            else
                hookEventArgs.response = of(functionResult);

            await this.hooks.emit(RpcHookType.PostCall, hookEventArgs);

            return hookEventArgs.response;
        }
        catch (err)
        {
            return throwError(err);
        }
    }
    
    private async handleRemoteFunctionCall(client: RpcClient, message: CallRpcMessage, extras: TExtras): Promise<void>
    {
        let obs = await this.doFunctionCall({
            parent: null,
            client,
            isLocal: false,
            name: message.name,
            args: message.args,
            metadata: message.metadata,
            handler: this,
            extras
        });

        const originalId = message.id;
        
        let remoteUnsubscribed = false;
        
        if (originalId)
        {
            obs = obs
                .pipe(
                    takeUntil(this.remoteUnsubscribe.pipe(
                        filter(value => value.client === client && equalsBinaryId(originalId, value.id)),
                        tap(() => remoteUnsubscribed = true)
                    ))
                );
        }
        
        obs
            .pipe(takeUntil(client.transport.closed))
            .subscribe({
                next: value => this.sendMessage(client, {
                    type: RpcMessageType.Next,
                    value,
                    originalId
                }),
                complete: () => {
                    // No need to send Complete when the remote is already unsubscribed
                    if (remoteUnsubscribed)
                        return;
                    
                    this.sendMessage(client, {
                        type: RpcMessageType.Complete,
                        originalId
                    })
                },
                error: error => this.sendMessage(client, {
                    type: RpcMessageType.Error,
                    error,
                    originalId
                })
            });
    }

    private handleUnsubscribe(client: RpcClient, id: RpcMessageId): void
    {
        this.remoteUnsubscribe.next({ client, id });
    }
    
    private sendMessage<TMessage extends SendRpcMessage>(client: RpcClient, message: TMessage): Promise<void>
    {
        return client.send(message);
    }
}