import {RpcEncoder} from '../rpc-encoder';
import {RpcMessage} from '../rpc-message';

export class PassthroughRpcEncoder implements RpcEncoder
{
    public encode(message: RpcMessage): any
    {
        return message;
    }

    public decode(data: any): RpcMessage
    {
        return data;
    }
}