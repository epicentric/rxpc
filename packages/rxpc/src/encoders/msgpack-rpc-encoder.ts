import {RpcEncoder} from '../rpc-encoder';
import {RpcMessage} from '../rpc-message';
import msgpack from 'msgpack-lite';

export class MsgpackRpcEncoder implements RpcEncoder
{
    public readonly codec: msgpack.Codec;
    
    public constructor()
    {
        this.codec = msgpack.createCodec({ preset: true });
    }

    public encode(message: RpcMessage): any
    {
        return msgpack.encode(message, { codec: this.codec });
    }
    
    public decode(data: any): RpcMessage
    {
        if (!(data instanceof Uint8Array))
        {
            if (data instanceof ArrayBuffer)
                data = new Uint8Array(data);
            else
                throw new Error(`Data is not an Uint8Array or ArrayBuffer!`);
        }

        return msgpack.decode(data, { codec: this.codec });
    }
}