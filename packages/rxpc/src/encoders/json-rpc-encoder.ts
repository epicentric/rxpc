import {RpcEncoder} from '../rpc-encoder';
import {RpcMessage} from '../rpc-message';
import {fromBinaryId, toBinaryId} from '@epicentric/binary-id';

export class JsonRpcEncoder implements RpcEncoder
{
    public encode({ id, type, originalId, ...otherProps }: RpcMessage): any
    {
        return JSON.stringify({
            type: type,
            id: id ? fromBinaryId(id) : id,
            originalId: originalId ? fromBinaryId(originalId) : originalId,
            ...otherProps
        });
    }
    
    public decode(data: any): RpcMessage
    {
        if (typeof data !== 'string')
            throw new Error(`Data is not a string!`);

        const { id, type, originalId, ...otherProps } = JSON.parse(data);
        
        return {
            type: type,
            originalId: originalId ? toBinaryId(originalId) : originalId,
            id: id ? toBinaryId(id) : id,
            ...otherProps
        } as RpcMessage;
    }
}