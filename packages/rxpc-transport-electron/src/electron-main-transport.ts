import {LastDisconnect, RpcTransport, RpcTransportDisconnectReason, RpcTransportStatus} from '@epicentric/rxpc';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {Event, ipcMain, WebContents} from 'electron';
import {map, take, takeUntil} from 'rxjs/operators';
import {BinaryId} from '@epicentric/binary-id';
import {filterIpcMessage} from './filter-ipc-message';

interface ElectronMainTransportOptions
{
    webContents: WebContents;
    id: BinaryId;
}

export class ElectronMainTransport implements RpcTransport
{
    public readonly closed: Subject<void> = new Subject;
    public readonly connectedChange: BehaviorSubject<boolean> = new BehaviorSubject(true);
    public readonly disconnected: Subject<any> = new Subject;
    public readonly error: Subject<any> = new Subject;
    public readonly reconnectionSupported = false;
    public readonly statusChange: BehaviorSubject<RpcTransportStatus> = new BehaviorSubject(RpcTransportStatus.Connected);
    public id: BinaryId;
    public message: Observable<unknown>;
    public webContents: WebContents;
    public sendingEnabled: boolean = true;
    public lastDisconnect: LastDisconnect = { reason: RpcTransportDisconnectReason.None, details: {} };

    public get status(): RpcTransportStatus
    {
        return this.statusChange.getValue();
    }

    private isClosed: boolean = false;

    public constructor(options: ElectronMainTransportOptions)
    {
        this.id = options.id;
        this.webContents = options.webContents;

        this.message = fromEvent<[Event, BinaryId, ...any[]]>(ipcMain, 'rpcMessage').pipe(
            filterIpcMessage(this.status, this.id),
            map(([event, id, data]) => data),
            takeUntil(this.closed)
        );
        
        fromEvent<[Event, BinaryId]>(ipcMain, 'rpcDisconnect').pipe(
            filterIpcMessage(this.status, this.id),
            take(1),
            takeUntil(this.closed)
        ).subscribe(() => this.close());
        
        // TODO: Remove these console.logs
        this.webContents.once('will-navigate', () => {
            console.log('Window navigating, closing rpc');
            this.close();
        });
        this.webContents.once('destroyed', () => {
            console.log('Window destroyed, closing rpc');
            this.close();
        });
    }

    public close(): void
    {
        if (this.isClosed)
            return;
        
        this.disconnect();
        
        this.isClosed = true;
        this.closed.next();
        this.closed.complete();
        this.connectedChange.complete();
        this.disconnected.complete();
        this.error.complete();
        this.statusChange.complete();
    }

    public connect(): Promise<void>
    {
        if (this.isClosed)
            throw new Error(`Can't connect with a closed transport`);
        
        this.ipcSend('rpcConnect');
        return Promise.resolve();
    }

    public disconnect(): void
    {
        if (this.status !== RpcTransportStatus.Connected)
            return;

        this.ipcSend('rpcDisconnect');
        this.statusChange.next(RpcTransportStatus.Disconnected);
    }

    public send(data: unknown): boolean
    {
        if (this.isClosed)
            return false;

        this.ipcSend('rpcMessage', data);
        return true;
    }
    
    private ipcSend(channel: string, data?: unknown): void
    {
        if (!this.sendingEnabled)
            return;
        
        if (data)
            this.webContents.send(channel, this.id, data);
        else
            this.webContents.send(channel, this.id);
    }
}