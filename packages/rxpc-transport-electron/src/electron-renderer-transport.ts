import {LastDisconnect, RpcTransport, RpcTransportDisconnectReason, RpcTransportStatus} from '@epicentric/rxpc';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {ipcRenderer} from 'electron';
import {map, takeUntil} from 'rxjs/operators';
import {BinaryId} from '@epicentric/binary-id';
import {filterIpcMessage} from './filter-ipc-message';

export interface ElectronRendererTransportOptions
{
    id: BinaryId;
}

export class ElectronRendererTransport implements RpcTransport
{
    public readonly closed: Subject<void> = new Subject;
    public readonly connectedChange: BehaviorSubject<boolean> = new BehaviorSubject(true);
    public readonly disconnected: Subject<any> = new Subject;
    public readonly error: Subject<any> = new Subject;
    public readonly reconnectionSupported = true;
    public readonly statusChange: BehaviorSubject<RpcTransportStatus> = new BehaviorSubject(RpcTransportStatus.Connected);
    public message: Observable<unknown>;
    public id: BinaryId;
    public lastDisconnect: LastDisconnect = { reason: RpcTransportDisconnectReason.None, details: {} };
    
    public get status(): RpcTransportStatus
    {
        return this.statusChange.getValue();
    }
    
    private isClosed: boolean = false;

    public constructor(options: ElectronRendererTransportOptions)
    {
        this.id = options.id;
        
        this.message = fromEvent<[Electron.Event, BinaryId, unknown]>(ipcRenderer, 'rpcMessage').pipe(
            filterIpcMessage(this.status, this.id),
            map(([event, id, data]) => data),
            takeUntil(this.closed)
        );
        
        fromEvent<[Electron.Event, BinaryId]>(ipcRenderer, 'rpcDisconnect').pipe(
            filterIpcMessage(this.status, this.id),
            takeUntil(this.closed)
        ).subscribe(() => this.statusChange.next(RpcTransportStatus.Disconnected));
    }
    
    public close(): void
    {
        if (this.isClosed)
            return;

        this.disconnect();
        
        this.isClosed = true;
        this.closed.next();
        this.closed.complete();
        this.connectedChange.complete();
        this.disconnected.complete();
        this.error.complete();
        this.statusChange.complete();
    }

    public async connect(): Promise<void>
    {
        if (this.isClosed)
            throw new Error(`Can't connect with a closed transport`);
        
        ipcRenderer.send('rpcConnect', this.id);
    }
    
    public disconnect(): void
    {
        if (this.status !== RpcTransportStatus.Connected)
            return;

        ipcRenderer.send('rpcDisconnect', this.id);
        this.statusChange.next(RpcTransportStatus.Disconnected);
    }

    public send(data: unknown): boolean
    {
        if (this.isClosed)
            return false;
        
        ipcRenderer.send('rpcMessage', this.id, data);
        return true;
    }
}