import {Event} from 'electron';
import {BinaryId, equalsBinaryId} from '@epicentric/binary-id';
import {RpcTransportStatus} from '@epicentric/rxpc';
import {filter} from 'rxjs/operators';

export function filterIpcMessage(status: RpcTransportStatus, id: BinaryId)
{
    return filter((args: [Event, BinaryId, ...any[]]) => {
        if (status !== RpcTransportStatus.Connected || !Array.isArray(args))
            return false;

        if (args[1] instanceof DataView && equalsBinaryId(args[1], id))
            return true;

        if (args[1] instanceof Buffer && equalsBinaryId(new DataView(args[1].buffer), id))
            return true;

        return false;
    });
}